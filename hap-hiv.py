#!/usr/bin/python

from __future__ import division
import logging
import sys
import os
import argparse
import pysam
import itertools
from Bio import SeqIO
import networkx as nx
from networkx.drawing.nx_agraph import to_agraph

class SNV_El:
    def __init__(self):
        self.pos = 0
        self.ref_nucl = 'N'
        self.cov = 0
        #self.reads = None
        self.alns = {'A' : None, 'C' : None, 'G' : None, 'T' : None, 'N' : None}

class CLONE_El:
    def __init__(self):
        self.start_pos = 0
        self.end_pos = 0
        self.cov = 0
        #self.reads = None
        self.clones = {}

def create_graph(variants, min_thr, out_dir):
    logging.info("Graph Construction")
    tikz_vdist = "3cm"
    tikz_hdist = "3cm"
    G = nx.DiGraph()
    tex_out = open(out_dir + "/graph.tex", "w")
    tex_out.write("\documentclass[11pt,a4paper]{article}\n")
    tex_out.write("\usepackage{color}\n")
    tex_out.write("\usepackage{tikz}\n")
    tex_out.write("\usetikzlibrary{positioning,calc}\n")
    tex_out.write("\usepackage[active,tightpage]{preview}\n")
    tex_out.write("\\begin{document}\n")
    tex_out.write("\\begin{figure}\n")
    tex_out.write("\\begin{preview}\n")
    tex_out.write("\\begin{tikzpicture}[node distance=" + tikz_vdist +
                  ", -, label position=right]\n")
    # Analyze SNVs
    first = 0
    last_nl = 'A'
    first_nl = 'A'
    for nl in variants[0].alns.keys():
        n_label = "\\shortstack{" + \
                  nl + " \\\ (" + str(len(variants[0].alns[nl])) + ")}"
        #n_label = nl
        if(nl == 'N'):
            continue
        if(first == 0):
            tex_out.write("\\node(" + \
                          str(variants[0].pos) + "_" + nl + \
                          ") at (0,0) {")
            if variants[0].ref_nucl == nl:
                tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
            else:
                tex_out.write(n_label)
            tex_out.write("};\n")
            first = 1
            first_nl = nl
        else:
            tex_out.write("\\node(" + \
                          str(variants[0].pos) + "_" + nl + \
                          ") [below of=" + \
                          str(variants[0].pos) + "_" + last_nl + "] {")
            if variants[0].ref_nucl == nl:
                tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
            else:
                tex_out.write(n_label)
            tex_out.write("};\n")
            last_nl = nl

    tex_out.write("\draw ($(" + \
                  str(variants[0].pos) + "_" + first_nl + \
                  ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                  str(variants[0].pos) + "_" + last_nl + \
                  ".south east)+(0.3,-0.3)$);\n");
    tex_out.write("\\node[above= 0.4cm of " + \
                  str(variants[0].pos) + "_A] {" + \
                  str(variants[0].pos) + "};\n")
    for sid in range(0, len(variants) - 1):
        sum_cov = 0
        for a in variants[sid].alns:
            sum_cov += len(variants[sid].alns[a])
        logging.debug("pos " + str(variants[sid].pos) +
                      " - ref " + variants[sid].ref_nucl +
                      " - cov " + str(variants[sid].cov) +
                      " [" +
                      ", ".join([x + ": " +
                                 str(len(variants[sid].alns[x])) for x in variants[sid].alns]) +
                      "]"
            )
        snv_curr = variants[sid].alns
        snv_next = variants[sid+1].alns
        first = 0
        for nl in variants[sid+1].alns.keys():
            n_label = "\\shortstack{" + nl + " \\\ (" + \
                      str(len(variants[sid+1].alns[nl])) + ")}"
            #n_label = nl
            if(nl == 'N'):
                continue
            if(first == 0):
                tex_out.write("\\node(" + \
                              str(variants[sid+1].pos) + "_" + nl + \
                              ") [right=" + tikz_hdist + " of " + \
                              str(variants[sid].pos) + "_" + first_nl + "] {")
                if variants[sid+1].ref_nucl == nl:
                    tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
                else:
                    tex_out.write(n_label)
                tex_out.write("};\n")
                first = 1
                first_nl = nl
                last_nl = nl
            else:
                tex_out.write("\\node(" + \
                              str(variants[sid+1].pos) + "_" + nl + \
                              ") [below of=" + \
                              str(variants[sid+1].pos) + "_" + last_nl + \
                              "] {")
                if variants[sid+1].ref_nucl == nl:
                    tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
                else:
                    tex_out.write(n_label)
                tex_out.write("};\n")
                last_nl = nl
        tex_out.write("\draw ($(" + \
                      str(variants[sid+1].pos) + "_" + first_nl + \
                      ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                      str(variants[sid+1].pos) + "_" + last_nl + \
                      ".south east)+(0.3,-0.3)$);\n")
        tex_out.write("\\node[above= 0.4cm of " + \
                      str(variants[sid+1].pos) + "_A] {" + \
                      str(variants[sid+1].pos) + "};\n")
        for curr_n in snv_curr:
            if(curr_n == 'N'):
                continue
            for next_n in snv_next:
                if(next_n == 'N'):
                    continue
                n1 = str(variants[sid].pos) + "_" + curr_n
                n2 = str(variants[sid+1].pos) + "_" + next_n
                edge_w = len(snv_curr[curr_n].intersection(snv_next[next_n]))
                logging.debug(n1 + " -> " + n2 + " : " + str(edge_w))
                # edge_w_norm = edge_w / (variants[sid].cov * 1.0)
                edge_w_norm = edge_w / (sum_cov * 1.0)
                #if edge_w > 10:
                if edge_w_norm > min_thr:
                    G.add_edge(n1, n2, weight = edge_w)
                    tex_out.write("\draw[->, >=stealth" +
                                  #",line width=" + str(edge_w / 200.0) + "pt" +
                                  "](" + n1 + ") -- (" + n2 + ")" +
                                  " node [sloped, pos=0.2, above=1pt] {" +
                                  str(round(edge_w_norm * 100,2)) + "\%" +
                                  #str(edge_w) +
                                  "}" + ";\n")
    tex_out.write("\end{tikzpicture}\n")
    tex_out.write("\end{preview}\n")
    tex_out.write("\end{figure}\n")
    tex_out.write("\end{document}\n")
    return G

def compute_init_clones_old(variants, left_best, right_best, best_common, min_thr):
    logging.info("Computing initial clones")
    al = []
    for p in range(left_best, right_best + 1):
        tmp = ""
        for a in variants[p].alns.keys():
            if len(variants[p].alns[a]) > 0:
                tmp += a
        al.append(tmp)
    logging.info(al)
    num_clones = 0
    for k in itertools.product(*al):
        num_clones += 1
        clone = set()
        clone_str = ""
        first = 0
        for p in range(left_best, right_best + 1):
            if first == 0:
                clone = variants[p].alns[k[p - left_best]]
                first = 1
            else:
                clone = clone.intersection(variants[p].alns[k[p - left_best]])
                if len(clone)/len(best_common) < min_thr:
                    break
            clone_str += k[p - left_best] + \
                         ": " + str(len(variants[p].alns[k[p - left_best]])) + " "

        clone_perc = len(clone)/len(best_common)
        if clone_perc >= min_thr:
            print(clone_str + " size: " + str(len(clone)) + \
                  " " + str(round(clone_perc * 100, 2)) + "%")
        #if num_clones % 1000000 == 0:
        #    print(str(num_clones))
    logging.debug("Tested combinations: " + str(num_clones))

def compute_init_clones(variants, left_best, right_best, num_best_common, min_thr, graph):
    logging.info("Computing initial clones")
    clones = CLONE_El()
    clones.start_pos = variants[left_best].pos
    clones.end_pos = variants[right_best].pos
    clones.cov = num_best_common

    logging.debug("Nodes: " + " ".join(list(graph.nodes())))
    logging.debug("Edges: ")
    logging.debug(graph.edges())
    cl = ["N"]*(right_best - left_best + 1)
    pos_ord = {}
    for p in range(left_best, right_best + 1):
        pos_ord[str(variants[p].pos)] = p
    # Initialize stack
    node_stack = []
    for n in variants[left_best].alns.keys():
        node = str(variants[left_best].pos) + "_" + n
        if node in graph.nodes():
            node_stack.append(node)
    head = ""
    num_disc = 0
    while len(node_stack) > 0:
        head = node_stack.pop()
        p,b = head.split("_")
        cl[pos_ord[p] - left_best] = b
        if p == str(variants[right_best].pos):
            cl_reads = set()
            first = 0
            for pos in range(left_best, right_best + 1):
                if first == 0:
                    cl_reads = variants[pos].alns[cl[pos - left_best]]
                    first = 1
                else:
                    cl_reads = cl_reads.intersection(variants[pos].alns[cl[pos - left_best]])
            cl_perc = len(cl_reads)/num_best_common
            if cl_perc >= min_thr:
                print("-".join(cl) + " size: " + str(len(cl_reads)) + \
                      " " + str(round(cl_perc * 100, 2)) + "% --> OK")
                clones.clones["".join(cl)] = cl_reads
            else:
                if len(cl_reads) > 0:
                    num_disc += 1
                    print("-".join(cl) + " size: " + str(len(cl_reads)) + \
                          " " + str(round(cl_perc * 100, 2)) + "%")
        else:
            if len(list(graph.neighbors(head))) > 0:
                for n in list(graph.neighbors(head)):
                    node_stack.append(n)
    logging.info("Num. Initial Clones: " + str(len(clones.clones)))
    logging.info("Num. Initial Clones Discarded: " + str(num_disc))
    return clones

def create_clone_graph(variants, min_thr, out_dir, clones, left_best, right_best):
    logging.info("Clone Graph Construction")
    tikz_vdist = "3cm"
    tikz_hdist = "3cm"
    G = nx.DiGraph()
    tex_out = open(out_dir + "/clone-graph.tex", "w")
    tex_out.write("\documentclass[11pt,a4paper]{article}\n")
    tex_out.write("\usepackage{color}\n")
    tex_out.write("\usepackage{tikz}\n")
    tex_out.write("\usetikzlibrary{positioning,calc}\n")
    tex_out.write("\usepackage[active,tightpage]{preview}\n")
    tex_out.write("\\begin{document}\n")
    tex_out.write("\\begin{figure}\n")
    tex_out.write("\\begin{preview}\n")
    tex_out.write("\\begin{tikzpicture}[node distance=" + tikz_vdist +
                  ", -, label position=right]\n")
    if left_best != 0:
        first = 0
        last_nl = 'A'
        first_nl = 'A'
        for nl in variants[0].alns.keys():
            n_label = "\\shortstack{" + nl + \
                      " \\\ (" + str(len(variants[0].alns[nl])) + ")}"
            if(nl == 'N'):
                continue
            if(first == 0):
                tex_out.write("\\node(" + \
                              str(variants[0].pos) + "-" + str(variants[0].pos) + \
                              "_" + nl + ") at (0,0) {" + n_label + "};\n")
                first = 1
                first_nl = nl
            else:
                tex_out.write("\\node(" + \
                              str(variants[0].pos) + "-" + \
                              str(variants[0].pos) + "_" + nl + \
                              ") [below of=" + \
                              str(variants[0].pos) + "-" + \
                              str(variants[0].pos) + "_" + last_nl + \
                              "] {" + n_label + "};\n")
                last_nl = nl
        tex_out.write("\draw ($(" + \
                      str(variants[0].pos) + "-" + \
                      str(variants[0].pos) + "_" + first_nl + \
                      ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                      str(variants[0].pos) + "-" + \
                      str(variants[0].pos) + "_" + last_nl + \
                      ".south east)+(0.3,-0.3)$);\n")
        tex_out.write("\\node[above= 0.4cm of " + \
                      str(variants[0].pos) + "-" + \
                      str(variants[0].pos) + "_A] {" + \
                      str(variants[0].pos) + "};\n")
        for sid in range(0, left_best - 1):
            sum_cov = 0
            for a in variants[sid].alns:
                sum_cov += len(variants[sid].alns[a])
            snv_curr = variants[sid].alns
            snv_next = variants[sid+1].alns
            first = 0
            for nl in variants[sid+1].alns.keys():
                n_label = "\\shortstack{" + nl + \
                          " \\\ (" + str(len(variants[sid+1].alns[nl])) + ")}"
                if(nl == 'N'):
                    continue
                if(first == 0):
                    tex_out.write("\\node(" + \
                                  str(variants[sid+1].pos) + "-" + \
                                  str(variants[sid+1].pos) + "_" + nl + \
                                  ") [right=" + tikz_hdist + " of " + \
                                  str(variants[sid].pos) + "-" + \
                                  str(variants[sid].pos) + "_" + first_nl + \
                                  "] {" + n_label + "};\n")
                    first = 1
                    first_nl = nl
                    last_nl = nl
                else:
                    tex_out.write("\\node(" + \
                                  str(variants[sid+1].pos) + "-" + \
                                  str(variants[sid+1].pos) + "_" + nl + \
                                  ") [below of=" + \
                                  str(variants[sid+1].pos) + "-" + \
                                  str(variants[sid+1].pos) + "_" + last_nl + \
                                  "] {" + n_label + "};\n")
                    last_nl = nl
            tex_out.write("\draw ($(" + \
                          str(variants[sid+1].pos) + "-" + \
                          str(variants[sid+1].pos) + "_" + first_nl + \
                          ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                          str(variants[sid+1].pos) + "-" + \
                          str(variants[sid+1].pos) + "_" + last_nl + \
                          ".south east)+(0.3,-0.3)$);\n")
            tex_out.write("\\node[above= 0.4cm of " + \
                          str(variants[sid+1].pos) + "-" + \
                          str(variants[sid+1].pos) + "_A] {" + \
                          str(variants[sid+1].pos) + "};\n")
            for curr_n in snv_curr:
                if(curr_n == 'N'):
                    continue
                for next_n in snv_next:
                    if(next_n == 'N'):
                        continue
                    n1 = str(variants[sid].pos) + "-" + \
                         str(variants[sid].pos) + "_" + curr_n
                    n2 = str(variants[sid+1].pos) + "-" + \
                         str(variants[sid+1].pos) + "_" + next_n
                    edge_w = len(snv_curr[curr_n].intersection(snv_next[next_n]))
                    # edge_w_norm = edge_w / (variants[sid].cov * 1.0)
                    edge_w_norm = edge_w / (sum_cov * 1.0)
                    #if edge_w > 12:
                    if edge_w_norm > min_thr:
                        G.add_edge(n1, n2, weight = edge_w)
                        tex_out.write("\draw[-" +
                                      #",line width=" + str(edge_w / 200.0) + "pt" +
                                      "](" + n1 + ") -- (" + n2 + ")" +
                                      " node [sloped, pos=0.2, above=1pt] {" +
                                      str(round(edge_w_norm * 100,2)) + "\%" +
                                      #str(edge_w) +
                                      "}" + ";\n")
    tex_out.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
    # Init clones
    kk = list(clones.clones.keys())
    first = 0
    if left_best == 0:
        first_nl = kk[0]
    last_nl = kk[0]
    for nl in kk:
        n_label = "\\shortstack{" + nl + " \\\ (" + str(len(clones.clones[nl])) + ")}"
        if(first == 0):
            if left_best == 0:
                tex_out.write("\\node(" + str(clones.start_pos) + "-" + \
                              str(clones.end_pos) + "_" + nl + \
                              ") at (0,0) {" + n_label + "};\n")
            else:
                tex_out.write("\\node(" + str(clones.start_pos) + "-" + \
                              str(clones.end_pos) + "_" + nl + \
                              ") [right=" + tikz_hdist + " of " + \
                              str(variants[left_best - 1].pos) + "-" + \
                              str(variants[left_best - 1].pos) + "_" + first_nl + \
                              "] {" + n_label + "};\n")
            first = 1
            first_nl = nl
        else:
            tex_out.write("\\node(" + \
                          str(clones.start_pos) + "-" + \
                          str(clones.end_pos) + "_" + nl + \
                          ") [below of=" + \
                          str(clones.start_pos) + "-" + \
                          str(clones.end_pos) + "_" + last_nl + \
                          "] {" + n_label + "};\n")
            last_nl = nl
    tex_out.write("\draw ($(" + \
                  str(clones.start_pos) + "-" + \
                  str(clones.end_pos) + "_" + first_nl + \
                  ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                  str(clones.start_pos) + "-" + \
                  str(clones.end_pos) +  "_" + last_nl + \
                  ".south east)+(0.3,-0.3)$);\n")
    tex_out.write("\\node[above= 0.4cm of " + \
                  str(clones.start_pos) + "-" + \
                  str(clones.end_pos) + "_" + first_nl + \
                  "] {" + str(clones.start_pos) + "-" + \
                  str(clones.end_pos) + "};\n")
    if left_best != 0:
        snv_curr = variants[left_best - 1].alns
        for curr_n in snv_curr:
            if(curr_n == 'N'):
                continue
            for next_n in kk:
                n1 = str(variants[left_best - 1].pos) + "-" + \
                     str(variants[left_best - 1].pos) + "_" + curr_n
                n2 = str(clones.start_pos) + "-" + \
                     str(clones.end_pos) + "_" + next_n
                edge_w = len(snv_curr[curr_n].intersection(clones.clones[next_n]))
                # edge_w_norm = edge_w / (variants[sid].cov * 1.0)
                edge_w_norm = edge_w / (sum_cov * 1.0)
                #if edge_w > 12:
                if edge_w_norm > min_thr:
                    G.add_edge(n1, n2, weight = edge_w)
                    tex_out.write("\draw[-" +
                                  #",line width=" + str(edge_w / 200.0) + "pt" +
                                  "](" + n1 + ") -- (" + n2 + ")" +
                                  " node [sloped, pos=0.2, above=1pt] {" +
                                  str(round(edge_w_norm * 100,2)) + "\%" +
                                  #str(edge_w) +
                                  "}" + ";\n")
    tex_out.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
    # Remaining variants
    for sid in range(right_best, len(variants) - 1):
        sum_cov = 0
        for a in variants[sid].alns:
            sum_cov += len(variants[sid].alns[a])
        snv_curr = variants[sid].alns
        snv_next = variants[sid+1].alns
        first = 0
        for nl in variants[sid+1].alns.keys():
            n_label = "\\shortstack{" + nl + " \\\ (" + \
                      str(len(variants[sid+1].alns[nl])) + ")}"
            if(nl == 'N'):
                continue
            if(first == 0):
                if sid == right_best:
                    tex_out.write("\\node(" + \
                                  str(variants[sid+1].pos) + "-" + \
                                  str(variants[sid+1].pos) + "_" + nl + \
                                  ") [right=" + tikz_hdist + " of " + \
                                  str(clones.start_pos) + "-" + \
                                  str(clones.end_pos) + "_" + first_nl + \
                                  "] {" + n_label + "};\n")
                else:
                    tex_out.write("\\node(" + \
                                  str(variants[sid+1].pos) + "-" + \
                                  str(variants[sid+1].pos) + "_" + nl + \
                                  ") [right=" + tikz_hdist + " of " + \
                                  str(variants[sid].pos) + "-" + \
                                  str(variants[sid].pos) + "_" + first_nl + \
                                  "] {" + n_label + "};\n")
                first = 1
                first_nl = nl
                last_nl = nl
            else:
                tex_out.write("\\node(" + \
                              str(variants[sid+1].pos) + "-" + \
                              str(variants[sid+1].pos) + "_" + nl + \
                              ") [below of=" + \
                              str(variants[sid+1].pos) + "-" + \
                              str(variants[sid+1].pos) + "_" + last_nl + \
                              "] {" + n_label + "};\n")
                last_nl = nl
        tex_out.write("\draw ($(" + \
                      str(variants[sid+1].pos) + "-" + \
                      str(variants[sid+1].pos) + "_" + first_nl + \
                      ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                      str(variants[sid+1].pos) + "-" + \
                      str(variants[sid+1].pos) + "_" + last_nl + \
                      ".south east)+(0.3,-0.3)$);\n")
        tex_out.write("\\node[above= 0.4cm of " + \
                      str(variants[sid+1].pos) + "-" + \
                      str(variants[sid+1].pos) + "_A] {" + \
                      str(variants[sid+1].pos) + "};\n")
        if sid != right_best:
            for curr_n in snv_curr:
                if(curr_n == 'N'):
                    continue
                for next_n in snv_next:
                    if(next_n == 'N'):
                        continue
                    n1 = str(variants[sid].pos) + "-" + \
                         str(variants[sid].pos) + "_" + curr_n
                    n2 = str(variants[sid+1].pos) + "-" + \
                         str(variants[sid+1].pos) + "_" + next_n
                    edge_w = len(snv_curr[curr_n].intersection(snv_next[next_n]))
                    # edge_w_norm = edge_w / (variants[sid].cov * 1.0)
                    edge_w_norm = edge_w / (sum_cov * 1.0)
                    #if edge_w > 12:
                    if edge_w_norm > min_thr:
                        G.add_edge(n1, n2, weight = edge_w)
                        tex_out.write("\draw[-" +
                                      #",line width=" + str(edge_w / 200.0) + "pt" +
                                      "](" + n1 + ") -- (" + n2 + ")" +
                                      " node [sloped, pos=0.2, above=1pt] {" +
                                      str(round(edge_w_norm * 100,2)) + "\%" +
                                      #str(edge_w) +
                                      "}" + ";\n")
        else:
            for curr_n in kk:
                for next_n in snv_next:
                    if(next_n == 'N'):
                        continue
                    n1 = str(clones.start_pos) + "-" + \
                         str(clones.end_pos) + "_" + curr_n
                    n2 = str(variants[sid+1].pos) + "-" + \
                         str(variants[sid+1].pos) + "_" + next_n
                    edge_w = len(clones.clones[curr_n].intersection(snv_next[next_n]))
                    # edge_w_norm = edge_w / (variants[sid].cov * 1.0)
                    edge_w_norm = edge_w / (sum_cov * 1.0)
                    #if edge_w > 12:
                    if edge_w_norm > min_thr:
                        G.add_edge(n1, n2, weight = edge_w)
                        tex_out.write("\draw[-" +
                                      #",line width=" + str(edge_w / 200.0) + "pt" +
                                      "](" + n1 + ") -- (" + n2 + ")" +
                                      " node [sloped, pos=0.2, above=1pt] {" +
                                      str(round(edge_w_norm * 100,2)) + "\%" +
                                      #str(edge_w) +
                                      "}" + ";\n")
    tex_out.write("\end{tikzpicture}\n")
    tex_out.write("\end{preview}\n")
    tex_out.write("\end{figure}\n")
    tex_out.write("\end{document}\n")
    return G

def main():
    parser = argparse.ArgumentParser(prog = "hap-hiv",
                                     description = "Compute HIV Quasispecies.",
                                     formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-b', '--bam-file',
                        help = "Alignment file in BAM format.",
                        required = True, dest = 'bam_file')
    parser.add_argument('-f', '--fasta-file',
                        help = "Reference sequence file in FASTA format.",
                        required = True, dest = 'fasta_file')
    parser.add_argument('-o', '--out-dir',
                        help = "Output directory.",
                        required = True, dest = 'out_dir')
    parser.add_argument('-r', '--region',
                        help = "Region in the form <chr>:<start>-<end>",
                        required = False, dest = 'region')
    parser.add_argument('-m', '--min-thr',
                        help = "Minimum threshold for SNV calling.",
                        required = False, dest = 'min_thr', type = float, default = 0.05)
    parser.add_argument('-v', '--verbose',
                        help='increase output verbosity',
                        action='count', default=0)
    args = parser.parse_args()
    
    if args.verbose == 0:
        log_level = logging.INFO
    elif args.verbose == 1:
        log_level = logging.DEBUG
    else:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level,
                        format='%(levelname)-8s [%(asctime)s]  %(message)s',
                        datefmt="%y%m%d %H%M%S")
    logging.info("Program Started")
    samfile = pysam.AlignmentFile(args.bam_file, "rb")
    # Parse FASTA Reference
    ref = {}
    if not os.path.isfile(args.fasta_file):
        logging.error("Reference FASTA file " + args.fasta_file + " not found!")
        exit(1)
    seq_handle = open(args.fasta_file, "r")
    for seq in SeqIO.parse(seq_handle, "fasta"):
        ref[seq.id] = seq.seq
    logging.info("Reference sequences: " + str(len(ref)))
    # Check if Region is given in input
    regs = {}
    for ref_name in ref.keys():
        if(samfile.get_tid(ref_name) != -1):
            regs[ref_name] = {'start' : 1, 'end' : len(ref[ref_name])}
    if args.region is not None:
        regs = {}
        chr,se = args.region.split(":")
        if chr not in ref or samfile.get_tid(chr) == -1:
            logging.error("Wrong input region.")
            exit(1)
        start,end = se.split("-")
        start = int(start)
        end = int(end)
        if start >= end:
            logging.error("Wrong input region.")
            exit(1)
        regs[chr] = {'start' : start, 'end' : end}
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)
    # Parse BAM File
    for ref_name in regs.keys():
        logging.info("Parsing Reads in " +
                     ref_name + ":" +
                     str(regs[ref_name]['start']) + "-" +
                     str(regs[ref_name]['end'])
        )
        variants = []
        ref_len = len(ref[ref_name])
        for pu_column in samfile.pileup(reference = ref_name,
                                        start = 1,
                                        end = len(ref[ref_name])):
            if pu_column.pos < regs[ref_name]['start'] - 1 or \
               pu_column.pos > regs[ref_name]['end'] - 1:
                continue
            snv = SNV_El()
            snv.pos = pu_column.pos
            snv.ref_nucl = ref[ref_name][pu_column.pos]
            snv.cov = pu_column.n
            #snv.reads = set()
            for nucl_base in snv.alns:
                snv.alns[nucl_base] = set()
            for pileupread in pu_column.pileups:
                if not pileupread.is_del and \
                   not pileupread.is_refskip and \
                   not pileupread.alignment.is_supplementary:
                    nucl = pileupread.alignment.query_sequence[pileupread.query_position]
                    snv.alns[nucl].add(pileupread.alignment.query_name)
                    #snv.reads.add(pileupread.alignment.query_name)
                    if pileupread.alignment.is_unmapped:
                        print("OK")
            n_over_thr = 0
            n_diff_ref = 0
            for n in snv.alns:
                if len(snv.alns[n]) > args.min_thr * pu_column.n and n != 'N':
                    n_over_thr += 1
                    if n != ref[ref_name][pu_column.pos]:
                        n_diff_ref += 1
            if n_over_thr >= 2 or n_diff_ref > 0:
                variants.append(snv)
        logging.info("Num. Variant Positions: " + str(len(variants)))
        for snv in variants:
            sum_covs = 0
            for x in snv.alns.keys():
                sum_covs += len(snv.alns[x]) 
            logging.debug("pos " + str(snv.pos) + \
                          " - ref " + snv.ref_nucl + \
                          " - covs [n:" + str(snv.cov) + \
                          #", reads:" + str(len(snv.reads)) + \
                          ", sum:" + str(sum_covs) + "]" + \
                          " [" + \
                          ", ".join([x + ":" + \
                                     str(len(snv.alns[x])) for x in snv.alns]) + \
                          "]"
            )
            # Check two reads supporting different variants in a position
            for n1 in snv.alns.keys():
                for n2 in snv.alns.keys():
                    if n1 == n2:
                        continue
                    inters = snv.alns[n1].intersection(snv.alns[n2])
                    #if len(inters) > 0:
                    #    print(str(snv.pos))
                    #    print(" ".join(inters))
                    #    exit(1)

        # Create the graph
        graph = create_graph(variants, args.min_thr, args.out_dir)
        
        # Compute base of the recurrence
        # 1 - get the closest 3 columns
        best = 1
        best_dist = (variants[1].pos + variants[0].pos) + \
                    (variants[2].pos - variants[1].pos)
        for i in range(1, len(variants) - 1):
            dist = (variants[i].pos - variants[i-1].pos) + \
                   (variants[i+1].pos - variants[i].pos)
            if dist < best_dist:
                best_dist = dist
                best = i
        logging.info("Closest columns: " + \
                     str(variants[best-1].pos) + " - " +
                     str(variants[best].pos) + " - " + \
                     str(variants[best+1].pos))
        # 2 - compute reads in common
        best_common = set()
        for i in range(-1, 2):
            num_r = ""
            #r_tot = set()
            nr_tot = set.union(*variants[best+i].alns.values())
            if len(best_common) == 0:
                best_common = nr_tot
            else:
                best_common = best_common.intersection(nr_tot)
            for b in variants[best+i].alns.keys():
                num_r += str(len(variants[best+i].alns[b])) + " "
                #r_tot = r_tot.union(variants[best+i].alns[b])
            logging.info(str(variants[best+i].pos) + \
                         ": " + num_r + " = "
                        #+ str(len(r_tot)) + " "
                        + str(len(nr_tot)) + " "
                        )
        logging.debug("Common: " + str(len(best_common)))
        logging.info("Extend column range")
        # 3 - try to extend it
        left_best = best - 1
        while left_best > 0:
            # Try to extend on left
            left_common = best_common.intersection(set.union(*variants[left_best - 1].alns.values()))
            logging.debug("Try Pos.: " + str(variants[left_best - 1].pos))
            logging.debug("B: " + str(len(best_common)))
            logging.debug("L: " + str(len(left_common)))
            logging.debug("%: " + str(len(left_common)/len(best_common)))
            if len(left_common)/len(best_common) >= 1 - args.min_thr:
                best_common = left_common
                left_best -= 1
            else:
                break
        right_best = best + 1
        while right_best < len(variants) - 1:
            # Try to exend on right
            right_common = best_common.intersection(set.union(*variants[right_best + 1].alns.values()))
            logging.debug("Try Pos.: " + str(variants[right_best + 1].pos))
            logging.debug("B: " + str(len(best_common)))
            logging.debug("R: " + str(len(right_common)))
            logging.debug("%: " + str(len(right_common)/len(best_common)))
            if len(right_common)/len(best_common) >= 1 - args.min_thr:
                best_common = right_common
                right_best += 1
            else:
                break
        logging.info("Left: " + str(variants[left_best].pos) + \
                     " Right: " + str(variants[right_best].pos))
        logging.info("Final Common: " + str(len(best_common)))
    samfile.close()
    # 4 - Compute inital clones
    init_clones = compute_init_clones(variants, \
                                      left_best, \
                                      right_best, \
                                      len(best_common), \
                                      args.min_thr, \
                                      graph)
    #compute_init_clones_old(variants, left_best, right_best, best_common, args.min_thr)
    create_clone_graph(variants, \
                       args.min_thr, \
                       args.out_dir, \
                       init_clones, \
                       left_best, \
                       right_best)
    # 5 - Extend Clones with Dynamic Programming
    
    logging.info("Program Finished")

if __name__ == "__main__":
    main() 
