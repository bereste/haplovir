#!/usr/bin/python

from __future__ import division
import logging
import sys
import os
import argparse
import pysam
import itertools
from Bio import SeqIO
import networkx as nx
#import pylab as plt
from networkx.drawing.nx_agraph import to_agraph
from networkx.drawing.nx_agraph import graphviz_layout

class SNV_El:
    def __init__(self):
        self.pos = 0
        self.ref_nucl = 'N'
        self.cov = 0
        #self.reads = None
        self.alns = {'A' : None, 'C' : None, 'G' : None, 'T' : None, 'N' : None}

class CLONE_El:
    def __init__(self):
        self.start_pos = 0
        self.end_pos = 0
        self.cov = 0
        #self.reads = None
        self.clones = {}

def create_graph(variants, min_thr, out_dir):
    logging.info("Graph Construction")
    tikz_vdist = "3cm"
    tikz_hdist = "3cm"
    G = nx.DiGraph()
    tex_out = open(out_dir + "/graph.tex", "w")
    tex_out.write("\documentclass[11pt,a4paper]{article}\n")
    tex_out.write("\usepackage{color}\n")
    tex_out.write("\usepackage{tikz}\n")
    tex_out.write("\usetikzlibrary{positioning,calc}\n")
    tex_out.write("\usepackage[active,tightpage]{preview}\n")
    tex_out.write("\\begin{document}\n")
    tex_out.write("\\begin{figure}\n")
    tex_out.write("\\begin{preview}\n")
    tex_out.write("\\begin{tikzpicture}[node distance=" + tikz_vdist +
                  ", -, label position=right]\n")
    # Analyze SNVs
    first = 0
    last_nl = 'A'
    first_nl = 'A'
    for nl in variants[0].alns.keys():
        n_label = "\\shortstack{" + \
                  nl + " \\\ (" + str(len(variants[0].alns[nl])) + ")}"
        #n_label = nl
        if(nl == 'N'):
            continue
        if(first == 0):
            tex_out.write("\\node(" + \
                          str(variants[0].pos) + "_" + nl + \
                          ") at (0,0) {")
            if variants[0].ref_nucl == nl:
                tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
            else:
                tex_out.write(n_label)
            tex_out.write("};\n")
            first = 1
            first_nl = nl
        else:
            tex_out.write("\\node(" + \
                          str(variants[0].pos) + "_" + nl + \
                          ") [below of=" + \
                          str(variants[0].pos) + "_" + last_nl + "] {")
            if variants[0].ref_nucl == nl:
                tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
            else:
                tex_out.write(n_label)
            tex_out.write("};\n")
            last_nl = nl

    tex_out.write("\draw ($(" + \
                  str(variants[0].pos) + "_" + first_nl + \
                  ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                  str(variants[0].pos) + "_" + last_nl + \
                  ".south east)+(0.3,-0.3)$);\n");
    tex_out.write("\\node[above= 0.4cm of " + \
                  str(variants[0].pos) + "_A] {" + \
                  str(variants[0].pos) + "};\n")
    max_w = 0
    for sid in range(0, len(variants) - 1):
        sum_cov = 0
        for a in variants[sid].alns:
            sum_cov += len(variants[sid].alns[a])
        logging.debug("pos " + str(variants[sid].pos) +
                      " - ref " + variants[sid].ref_nucl +
                      " - cov " + str(variants[sid].cov) +
                      " [" +
                      ", ".join([x + ": " +
                                 str(len(variants[sid].alns[x])) for x in variants[sid].alns]) +
                      "]"
            )
        snv_curr = variants[sid].alns
        snv_next = variants[sid+1].alns
        first = 0
        for nl in variants[sid+1].alns.keys():
            n_label = "\\shortstack{" + nl + " \\\ (" + \
                      str(len(variants[sid+1].alns[nl])) + ")}"
            #n_label = nl
            if(nl == 'N'):
                continue
            if(first == 0):
                tex_out.write("\\node(" + \
                              str(variants[sid+1].pos) + "_" + nl + \
                              ") [right=" + tikz_hdist + " of " + \
                              str(variants[sid].pos) + "_" + first_nl + "] {")
                if variants[sid+1].ref_nucl == nl:
                    tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
                else:
                    tex_out.write(n_label)
                tex_out.write("};\n")
                first = 1
                first_nl = nl
                last_nl = nl
            else:
                tex_out.write("\\node(" + \
                              str(variants[sid+1].pos) + "_" + nl + \
                              ") [below of=" + \
                              str(variants[sid+1].pos) + "_" + last_nl + \
                              "] {")
                if variants[sid+1].ref_nucl == nl:
                    tex_out.write("\\textcolor{red}{\\bf " + n_label + "}")
                else:
                    tex_out.write(n_label)
                tex_out.write("};\n")
                last_nl = nl
        tex_out.write("\draw ($(" + \
                      str(variants[sid+1].pos) + "_" + first_nl + \
                      ".north west)" + "+(-0.3,0.3)$) rectangle ($(" + \
                      str(variants[sid+1].pos) + "_" + last_nl + \
                      ".south east)+(0.3,-0.3)$);\n")
        tex_out.write("\\node[above= 0.4cm of " + \
                      str(variants[sid+1].pos) + "_A] {" + \
                      str(variants[sid+1].pos) + "};\n")
        for curr_n in snv_curr:
            if(curr_n == 'N'):
                continue
            for next_n in snv_next:
                if(next_n == 'N'):
                    continue
                n1 = str(variants[sid].pos) + "_" + curr_n
                n2 = str(variants[sid+1].pos) + "_" + next_n
                edge_w = len(snv_curr[curr_n].intersection(snv_next[next_n]))
                logging.debug(n1 + " -> " + n2 + " : " + str(edge_w))
                edge_w_norm = edge_w / (sum_cov * 1.0)
                #if edge_w > 10:
                if edge_w_norm > min_thr:
                    if edge_w_norm > max_w:
                        max_w = edge_w_norm
                    #G.add_edge(n1, n2, capacity = edge_w, label = str(edge_w))
                    G.add_edge(n1, n2,
                               capacity = edge_w_norm,
                               label = str(round(edge_w_norm * 100, 2)),
                               flow = 0)
                    tex_out.write("\draw[->, >=stealth" +
                                  "](" + n1 + ") -- (" + n2 + ")" +
                                  " node [sloped, pos=0.2, above=1pt] {" +
                                  str(round(edge_w_norm * 100,2)) + "\%" +
                                  #str(edge_w) +
                                  "}" + ";\n")
    # Add source node
    for n in variants[0].alns:
        if str(variants[0].pos) + "_" + n in G:
            G.add_edge("S", str(variants[0].pos) + "_" + n,
                       capacity = max_w,
                       label = str(round(max_w * 100, 2)),
                       flow = 0)
    # Add sink node
    last_idx = len(variants) - 1
    for n in variants[last_idx].alns:
        if str(variants[last_idx].pos) + "_" + n in G:
            G.add_edge(str(variants[last_idx].pos) + "_" + n, "T",
                       capacity = max_w,
                       label = str(round(max_w * 100, 2)),
                       flow = 0)
    
    tex_out.write("\end{tikzpicture}\n")
    tex_out.write("\end{preview}\n")
    tex_out.write("\end{figure}\n")
    tex_out.write("\end{document}\n")
    return G

# ####################################
# ## Construct clones from Max Flow ##
# ## OLD WAY                        ##
# ####################################
# def computeMaxFlowAllPaths(graph, stack, flow, path, max_flow):
#     v = stack[len(stack)-1]
#     if v == "T":
#         #print(" ".join(stack) + "  " + str(flow))
#         if flow > max_flow:
#             return " ".join(stack), flow
#         else:
#             return path, max_flow
#     else:
#         for n in graph[v]:
#             stack.append(n)
#             arc_flow = graph[v][n]['flow']
#             if arc_flow < flow:
#                 path, max_flow = computeMaxFlowAllPaths(graph,
#                                                         stack,
#                                                         arc_flow,
#                                                         path,
#                                                         max_flow)
#             else:
#                 path, max_flow = computeMaxFlowAllPaths(graph,
#                                                         stack,
#                                                         flow,
#                                                         path,
#                                                         max_flow)
#             while stack[len(stack) -1] != v:
#                 stack.pop()
#         return path, max_flow

####################################
## Construct clones from Max Flow ##
####################################
def computeMaxFlowPaths(graph):
    clones = {}
    new_path = 1
    while new_path != 0:
        new_path = 0
        s,f = computeMaxFlowMaxPath(graph, ["S"], 1)
        clones[" ".join(s)] = round(f * 100, 4)
        for n in graph["S"]:
            if graph["S"][n]["flow"] > 0:
                new_path = 1
    return clones

def computeMaxFlowMaxPath(graph, stack, flow):
    v = stack[len(stack)-1]
    if v == "T":
        for p in range(1,len(stack)):
            graph[stack[p-1]][stack[p]]["flow"] -= flow
        return stack,flow
    else:
        max_arc_flow = 0
        max_arc = ""
        for n in graph[v]:
            if graph[v][n]["flow"] > max_arc_flow:
                max_arc_flow = graph[v][n]["flow"]
                max_arc = n
        stack.append(max_arc)
        if max_arc_flow < flow:
            flow = max_arc_flow
        stack,flow = computeMaxFlowMaxPath(graph, stack, flow)
        return stack,flow

# ############################################
# ## Construct clones from Augumented Paths ##
# ## OLD WAY                                ##
# ############################################
# def computeAPFlow(graph, stack, flow, path, max_flow):
#     v = stack[len(stack)-1]
#     if v == "T":
#         #print(" ".join(stack) + "  " + str(flow))
#         if flow > max_flow:
#             return " ".join(stack), flow
#         else:
#             return path, max_flow
#     else:
#         for n in graph[v]:
#             stack.append(n)
#             arc_cap = graph[v][n]['capacity']
#             if arc_cap < flow:
#                 path, max_flow = computeAPFlow(graph,
#                                                stack,
#                                                arc_cap,
#                                                path,
#                                                max_flow)
#             else:
#                 path, max_flow = computeAPFlow(graph,
#                                                stack,
#                                                flow,
#                                                path,
#                                                max_flow)
#             while stack[len(stack) -1] != v:
#                 stack.pop()
#         return path, max_flow

############################################
## Construct clones from Augumented Paths ##
############################################
def computeAugPaths(graph):
    clones = {}
    new_path = 1
    while new_path != 0:
        new_path = 0
        s,f = computeMaxAugPath(graph, ["S"], 1, "", 0)
        if f > 0:
            for p in range(1,len(s)):
                graph[s[p-1]][s[p]]["flow"] += f
            clones[" ".join(s)] = round(f * 100, 4)
            new_path = 1
    return clones

def computeMaxAugPath(graph, stack, flow, max_path, max_flow):
    v = stack[len(stack)-1]
    if v == "T":
        if flow > max_flow:
            max_flow = flow
            max_path = list(stack)
        return max_path,max_flow
    else:
        for n in graph[v]:
            stack.append(n)
            arc_cap = graph[v][n]["capacity"] - graph[v][n]["flow"]
            if arc_cap < flow:
                max_path, max_flow = computeMaxAugPath(graph,
                                               stack,
                                               arc_cap,
                                               max_path,
                                               max_flow)
            else:
                max_path, max_flow = computeMaxAugPath(graph,
                                               stack,
                                               flow,
                                               max_path,
                                               max_flow)
            while stack[len(stack) -1] != v:
                stack.pop()
        return max_path,max_flow

def main():
    parser = argparse.ArgumentParser(prog = "hap-hiv",
                                     description = "Compute HIV Quasispecies.",
                                     formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-b', '--bam-file',
                        help = "Alignment file in BAM format.",
                        required = True, dest = 'bam_file')
    parser.add_argument('-f', '--fasta-file',
                        help = "Reference sequence file in FASTA format.",
                        required = True, dest = 'fasta_file')
    parser.add_argument('-o', '--out-dir',
                        help = "Output directory.",
                        required = True, dest = 'out_dir')
    parser.add_argument('-r', '--region',
                        help = "Region in the form <chr>:<start>-<end>",
                        required = False, dest = 'region')
    parser.add_argument('-m', '--min-thr',
                        help = "Minimum threshold for SNV calling.",
                        required = False, dest = 'min_thr', type=float, default = 0.05)
    parser.add_argument('-a', '--algo',
                        help = "Algorithm: max-flow or aug-paths",
                        required = False, dest = 'algo', type=str, default = "max-flow")
    parser.add_argument('-v', '--verbose',
                        help='increase output verbosity',
                        action='count', default=0)
    args = parser.parse_args()
    
    if args.verbose == 0:
        log_level = logging.INFO
    elif args.verbose == 1:
        log_level = logging.DEBUG
    else:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level,
                        format='%(levelname)-8s [%(asctime)s]  %(message)s',
                        datefmt="%y%m%d %H%M%S")
    logging.info("Program Started")
    if args.algo != "max-flow" and args.algo != "aug-paths":
        logging.error("Wrong algorithm option!")
        logging.error("Valid options: 'max-flow' (maximum flow) and 'aug-paths' (augumented paths)")
        exit(1)
    samfile = pysam.AlignmentFile(args.bam_file, "rb")
    # Parse FASTA Reference
    ref = {}
    if not os.path.isfile(args.fasta_file):
        logging.error("Reference FASTA file " + args.fasta_file + " not found!")
        exit(1)
    seq_handle = open(args.fasta_file, "r")
    for seq in SeqIO.parse(seq_handle, "fasta"):
        ref[seq.id] = seq.seq
    logging.info("Reference sequences: " + str(len(ref)))
    # Check if Region is given in input
    regs = {}
    for ref_name in ref.keys():
        if(samfile.get_tid(ref_name) != -1):
            regs[ref_name] = {'start' : 1, 'end' : len(ref[ref_name])}
    if args.region is not None:
        regs = {}
        chr,se = args.region.split(":")
        if chr not in ref or samfile.get_tid(chr) == -1:
            logging.error("Wrong input region.")
            exit(1)
        start,end = se.split("-")
        start = int(start)
        end = int(end)
        if start >= end:
            logging.error("Wrong input region.")
            exit(1)
        regs[chr] = {'start' : start, 'end' : end}
    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)
    # Parse BAM File
    for ref_name in regs.keys():
        logging.info("Parsing Reads in " +
                     ref_name + ":" +
                     str(regs[ref_name]['start']) + "-" +
                     str(regs[ref_name]['end'])
        )
        variants = []
        ref_len = len(ref[ref_name])
        for pu_column in samfile.pileup(reference = ref_name,
                                        start = 1,
                                        end = len(ref[ref_name])):
            if pu_column.pos < regs[ref_name]['start'] - 1 or \
               pu_column.pos > regs[ref_name]['end'] - 1:
                continue
            snv = SNV_El()
            snv.pos = pu_column.pos
            snv.ref_nucl = ref[ref_name][pu_column.pos]
            snv.cov = pu_column.n
            #snv.reads = set()
            for nucl_base in snv.alns:
                snv.alns[nucl_base] = set()
            for pileupread in pu_column.pileups:
                if not pileupread.is_del and \
                   not pileupread.is_refskip and \
                   not pileupread.alignment.is_supplementary:
                    nucl = pileupread.alignment.query_sequence[pileupread.query_position]
                    snv.alns[nucl].add(pileupread.alignment.query_name)
                    #snv.reads.add(pileupread.alignment.query_name)
                    if pileupread.alignment.is_unmapped:
                        print("OK")
            n_over_thr = 0
            n_diff_ref = 0
            for n in snv.alns:
                if len(snv.alns[n]) > args.min_thr * pu_column.n and n != 'N':
                    n_over_thr += 1
                    if n != ref[ref_name][pu_column.pos]:
                        n_diff_ref += 1
            if n_over_thr >= 2 or n_diff_ref > 0:
                variants.append(snv)
        logging.info("Num. Variant Positions: " + str(len(variants)))
        for snv in variants:
            sum_covs = 0
            for x in snv.alns.keys():
                sum_covs += len(snv.alns[x]) 
            logging.debug("pos " + str(snv.pos) + \
                          " - ref " + snv.ref_nucl + \
                          " - covs [n:" + str(snv.cov) + \
                          #", reads:" + str(len(snv.reads)) + \
                          ", sum:" + str(sum_covs) + "]" + \
                          " [" + \
                          ", ".join([x + ":" + \
                                     str(len(snv.alns[x])) for x in snv.alns]) + \
                          "]"
            )
            # Check two reads supporting different variants in a position
            for n1 in snv.alns.keys():
                for n2 in snv.alns.keys():
                    if n1 == n2:
                        continue
                    inters = snv.alns[n1].intersection(snv.alns[n2])
                    #if len(inters) > 0:
                    #    print(str(snv.pos))
                    #    print(" ".join(inters))
                    #    exit(1)

        # Create the graph
        graph = create_graph(variants, args.min_thr, args.out_dir)

        clones = {}
        ##################
        ## Maximum Flow ##
        ##################
        if args.algo == "max-flow":
            print("## MAX FLOW ##")
            flow_val, flow_dict = nx.maximum_flow(graph, "S", "T")
            print("Max. Flow: " + str(round(flow_val * 100, 4)))
            #print(flow_dict)

            for (s,e) in graph.edges():
                graph[s][e]["flow"] = flow_dict[s][e]

            # Visualize Graph
            for (s,e) in graph.edges():
                graph[s][e]["label"] = str(round(graph[s][e]["flow"] * 100, 2)) + \
                                       "/" + \
                                       graph[s][e]["label"]
            A = to_agraph(graph)
            #print(A)
            A.layout('neato', args = "-Goverlap=scale")
            A.draw(args.out_dir + "/GrafoMxFlow.png")

            clones = computeMaxFlowPaths(graph)

            ## OLD WAY ##
            # mf = 1
            # tot_flow = 0
            # while mf != 0:
            #     path,mf = computeMaxFlowAllPaths(graph, ["S"], 1, "", 0)
            #     tot_flow += mf
            #     if mf != 0:
            #         print(path + "  " + str(round(mf * 100, 4)))
            #         path = path.split(" ")
            #         for p in range(1,len(path)):
            #             graph[path[p-1]][path[p]]["flow"] -= mf
            # print("Tot. Flow: " + str(round(tot_flow * 100, 4)))

        # ######################
        # ## Augumented Paths ##
        # ######################
        if args.algo == "aug-paths":
            print("## AUG PATHS ##")
            for (s,e) in graph.edges():
                graph[s][e]["flow"] = 0

            clones = computeAugPaths(graph)

            ## OLD WAY ##
            # mf = 1
            # tot_flow = 0
            # while mf != 0:
            #     path,mf = computeAPFlow(graph, ["S"], 1, "", 0)
            #     tot_flow += mf
            #     if mf != 0:
            #         print(path + "  " + str(round(mf * 100, 4)))
            #         path = path.split(" ")
            #         for p in range(1,len(path)):
            #             graph[path[p-1]][path[p]]["capacity"] -= mf
            #             graph[path[p-1]][path[p]]["flow"] += mf
            # print("Tot. Flow: " + str(round(tot_flow * 100, 4)))

            # Visualize Graph
            for (s,e) in graph.edges():
                graph[s][e]["label"] = str(round(graph[s][e]["flow"] * 100, 2)) + \
                                 "/" + \
                                 graph[s][e]["label"]
            A = to_agraph(graph)
            #print(A)
            A.layout('neato', args = "-Goverlap=scale")
            A.draw(args.out_dir + "/GrafoAugPaths.png")

        ## Print clones ##
        tot_flow = 0
        for c in clones.keys():
            if clones[c] >= (args.min_thr * 100):
                tot_flow += clones[c]
                print(c + " " + str(clones[c]))
        print("Tot. Flow: " + str(tot_flow))

    logging.info("Program Finished")

if __name__ == "__main__":
    main() 
